# Hangman Game

import random
word_list = ["recitation", "valuation", "testimony"]
chosen_word = random.choice(word_list)

word_length = len(chosen_word)
# Testing
print("The solution is ",chosen_word)

# TODO-1: Create an empty List called display and for each letter in choosen_word, add a "_" to display
# eg: "apple" -> ["_","_","_","_","_"]


guess = input("Guess a letter: ").lower()

# TODO-2: Loop through each position in chosen_word and if the letter at that position matches 'guess' then reveal the letter in the display at that position.
for position in range(word_length):
    letter = chosen_word[position]
    


# TODO-3: Print 'display' and you should see the guessed letter in correct positon and every other letter replace with "_".
